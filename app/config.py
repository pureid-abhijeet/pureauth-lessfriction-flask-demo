import os
import secrets

CONFIG_BASEDIR = os.path.dirname(os.path.abspath(__file__))


class BaseConfig:
    SECRET_KEY = secrets.token_hex(30)
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
    SQLALCHEMY_TRACK_MODIFICATIONS = bool(
        os.environ.get("SQLALCHEMY_TRACK_MODIFICATIONS", False)
    )


class ProductionConfig(BaseConfig):
    pass


class DevelopmentConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        "DATABASE_URL",
        "sqlite:///{}".format(os.path.join(CONFIG_BASEDIR, "database.db")),
    )


class TestConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(
        os.path.join(CONFIG_BASEDIR, "database.db")
    )

from datetime import datetime

from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required, login_user, logout_user
from pureauth_sdk import exceptions, generate_ecies_qr, load_public_key
from werkzeug.security import check_password_hash

from app import db
from app.models import User

home = Blueprint("home", __name__)


@home.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        email = request.form.get("email")
        user = User.query.filter_by(email=email).first()
        if user:
            if "password-login" in request.form:
                return redirect(
                    url_for("home.password_login", email=request.form.get("email"))
                )
            elif "pureauth-login" in request.form:
                if user.public_key:
                    return redirect(
                        url_for("home.pureauth_login", email=request.form.get("email"))
                    )
                else:
                    flash(
                        "Public Key missing! To use PureAuth authentication upload your public key first using Password Login"
                    )
            else:
                pass
        else:
            flash("User not exists!")

    return render_template("home/index.html")


@home.route("/register", methods=["GET", "POST"])
def register():
    if request.method == "POST":
        name = request.form.get("name")
        email = request.form.get("email")
        password = request.form.get("password")

        email_check = User.query.filter_by(email=email).first()

        if email_check:
            flash("User already exists")
        else:
            user = User(name=name, email=email, password=password)
            db.session.add(user)
            db.session.commit()

            flash("User added Successfully!")
            return redirect(url_for("home.index"))

    else:
        if current_user.is_authenticated:
            return redirect(url_for("home.index"))

    return render_template("home/register.html")


@home.route("/auth/password", methods=["GET", "POST"])
def password_login():
    if request.method == "POST":
        email = request.form.get("email")
        password = request.form.get("password")

        email_user = User.query.filter_by(email=email).first()
        if email_user:
            password_user = check_password_hash(email_user.password, password)

            if password_user:
                login_user(email_user)
                return redirect(url_for("home.dashboard"))
            else:
                flash("Wrong Credentials!")
        else:
            flash("User not exists!")

    else:
        if current_user.is_authenticated:
            return redirect(url_for("home.dashboard"))

        if request.args.get("email") is None:
            flash("Please enter your Email ID")
            return redirect(url_for("home.index"))

    return render_template("home/password_login.html")


@home.route("/auth/pureauth", methods=["GET", "POST"])
def pureauth_login():
    if request.method == "POST":
        email = request.form.get("email")
        otp = request.form.get("otp")
        user = User.query.filter_by(email=email, otp=otp).first()
        if user:
            login_user(user)
            return redirect(url_for("home.dashboard"))
        else:
            flash("Wrong Credentails")
            return redirect(url_for("home.index"))
    else:
        if current_user.is_authenticated:
            return redirect(url_for("home.dashboard"))

        if request.args.get("email") is None:
            flash("Please enter Email ID")
            return redirect(url_for("home.index"))
        email = request.args.get("email")
        qr_user = User.query.filter_by(email=email).first()
        qr_code_data = generate_ecies_qr(qr_user.public_key)
        qr_user.otp = qr_code_data[1].decode("utf-8")
        qr_user.otp_created_at = datetime.utcnow()

        db.session.add(qr_user)
        db.session.commit()

    return render_template("home/pureauth_login.html", qr_code=qr_code_data[0])


@home.route("/dashboard", methods=["GET", "POST"])
@login_required
def dashboard():
    if request.method == "POST":
        public_key = request.form.get("public_key")
        try:
            load_public_key(public_key)
            user = User.query.get(current_user.id)
            user.public_key = public_key
            db.session.add(user)
            db.session.commit()
            flash("Public Key uploaded successfully!")
        except exceptions.InvalidECPublicKey:
            flash("Unable to upload Public Key")

    return render_template("home/dashboard.html")


@home.route("/auth/logout")
def logout():
    logout_user()
    return redirect(url_for("home.index"))

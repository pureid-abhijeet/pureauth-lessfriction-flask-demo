from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

db = SQLAlchemy()
migrate = Migrate()
login_manager = LoginManager()
login_manager.login_view = "home.index"


def create_app(config="app.config.ProductionConfig"):
    app = Flask(__name__)
    app.config.from_object(config)
    db.init_app(app)

    migrate.init_app(app, db, directory="app/db/migrations")

    from app.models import User
    from app.views.home import home

    login_manager.init_app(app)

    app.register_blueprint(home)

    return app
